package com.zhonghan.bookstore_byq;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Demo2 {

	public static void main(String[] args) {
		createConn();

		for (int i = 1; i <= 100; i++) {
			String istr = String.format("%03d", i);
			exeUpdate("Jack" + istr, String.valueOf(i), "Java96");
		}
		exeQuery("Jack");
		
		closeConn();
	}

	private static Connection conn;

	/**
	 * 创建连接
	 */
	public static void createConn() {
		try {
			conn = null;
			String url = "jdbc:mysql://localhost:3306/demo2";
			String user = "root";
			String password = "123456";
			// 1. 选择数据库：加载数据库驱动
			Class.forName("com.mysql.jdbc.Driver");
			// 2. 连接数据库
			conn = DriverManager.getConnection(url, user, password);
		} catch (ClassNotFoundException e) {
			System.out.println("数据库驱动没有找到");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void exeQuery(String name) {
		PreparedStatement ps = null;
		try {
			// 创建数据库查询
			ps = conn.prepareStatement("SELECT id,name,code,clazz FROM demo1 WHERE name LIKE ?");
			ps.setString(1, name + "%");
			// 获取查询结果
			ResultSet rs = ps.executeQuery();
			// 遍历结果
			while (rs.next()) {
				int id = rs.getInt("id");
				String name1 = rs.getString("name");
				String code = rs.getString("code");
				String clazz = rs.getString("clazz");
				System.out.println("id="+id+",name="+name1+",code="+code+",clazz="+clazz+"");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}	finally {
			// 5. 关闭查询
			try {
				if (null != ps) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void exeUpdate(String name, String code, String clazz) {
		PreparedStatement ps = null;
		try {
			// 创建数据库查询
			ps = conn.prepareStatement("INSERT INTO demo1(name,code,clazz) VALUES(?,?,?)");
			ps.setString(1, name);
			ps.setString(2, code);
			ps.setString(3, clazz);
			// 获取查询结果
			int i = ps.executeUpdate();
			System.out.println("一共执行了" + i + "条");
		} catch (SQLException e) {
			e.printStackTrace();
		}	finally {
			// 5. 关闭查询
			try {
				if (null != ps) {
					ps.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 关闭连接
	 */
	public static void closeConn() {
		// 5. 关闭连接
		try {
			if (null != conn) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
