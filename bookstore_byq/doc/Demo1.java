package com.zhonghan.bookstore_byq;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.*;

public class Demo1 {
	public static void main(String[] args) {
		insert("Tom", "1", "Java96");
	}

	public static void insert(String name, String code, String clazz) {
		Connection conn = null;
		PreparedStatement ps = null;
		String url = "jdbc:mysql://localhost:3306/demo2";
		String user = "root";
		String password = "123456";

		try {
			// 1. 选择数据库：加载数据库驱动
			Class.forName("com.mysql.jdbc.Driver");
			// 2. 连接数据库
			conn = DriverManager.getConnection(url, user, password);
			// 3. 创建数据库查询
			ps = conn.prepareStatement("INSERT1 INTO demo1(name,code,clazz) VALUES(?,?,?)");
			ps.setString(1, name);
			ps.setString(2, code);
			ps.setString(3, clazz);
			// 4. 获取查询结果
			int i = ps.executeUpdate();
			System.out.println("一共执行了" + i + "条");
		} catch (ClassNotFoundException e) {
			System.out.println("数据库驱动没有找到");
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 5. 关闭查询和连接
			try {
				if (null != ps) {
					ps.close();
				}
				if (null != conn) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}