package com.zhonghan.misc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/compute")
public class ComputerServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int res = 0;
		String x = req.getParameter("x");
		String y = req.getParameter("y");
		String operator = req.getParameter("operator");

		int ix = Integer.parseInt(x);
		int iy = Integer.parseInt(y);
		if (operator.equals("add")) {
			res = ix + iy;
		}

		ServletOutputStream out = resp.getOutputStream();
		out.write(("" + res).getBytes());

		out.flush();
		out.close();

	}

	@Override
	protected void doPost(HttpServletRequest req,

			HttpServletResponse resp) throws ServletException, IOException {

		this.doGet(req, resp);

	}

}