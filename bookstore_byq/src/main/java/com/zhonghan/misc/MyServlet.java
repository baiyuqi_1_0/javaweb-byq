package com.zhonghan.misc;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
@WebServlet("/myservlet")
public class MyServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession();
		 Object counter = session.getAttribute("counter");
		 if(counter == null) {
			 counter = new Integer(0);
		 }
		 Integer IC = (Integer) counter;
		 IC++;
		 session.setAttribute("counter", IC);
		
		
		InputStream in = new FileInputStream("e:/cat.jpg");
	
	
		ServletOutputStream out = resp.getOutputStream();
		
		
		
		byte[] buffer = new byte[2048];
		while(true) {
			int l = in.read(buffer);
			if(l < 0)
				break;
			out.write(buffer,0,l);
		}
		out.flush();
		out.close();
	
		
/*
		resp.getWriter().write("jjjj");
		resp.getWriter().flush();

		resp.getWriter().close();
		*/
		

	}

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		super.service(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req,

			HttpServletResponse resp) throws ServletException, IOException {

		this.doGet(req, resp);

	}


}