package com.zhonghan.bookstore.account.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhonghan.bookstore.account.model.User;
import com.zhonghan.bookstore.account.service.UserManagerJPA;
import com.zhonghan.bookstore.log.model.Log;
import com.zhonghan.bookstore.log.service.LogService;
import com.zhonghan.bookstore.security.Result;

@RestController

public class LoginController {
	@Autowired
	UserManagerJPA manager;
	
	@Autowired
	LogService logService;

	@GetMapping("/login")
	public Result login(String name, String password) {
		
		
		
		// 1. 查找用户
		User user = manager.fetch(name, password);
		if (user == null) {
			return new Result(false, null, "refused!");
		}
		// 2. 根据用户标识，拿到所有权限
		List<String> permissions = manager.extractPermissions(user.getId());
		
		
		
        //3. 将所有权限放入SecuityContext上下文
		Collection<GrantedAuthority> authorities = authorities(permissions);

		Authentication authentication = new UsernamePasswordAuthenticationToken(name, password, authorities);

		SecurityContext scontext = SecurityContextHolder.getContext();

		scontext.setAuthentication(authentication);
		
		

		return new Result(true, permissions, "success!");
	}
	
	Collection<GrantedAuthority> authorities(List<String> permissions){
		Collection<GrantedAuthority> authorities = new ArrayList();// authentication.getAuthorities();

		for (String p : permissions) {
			GrantedAuthority aut = new SimpleGrantedAuthority(p);
			authorities.add(aut);

		}
		return authorities;

		
	}
	@GetMapping("/ini")
	public Result ini() {
		manager.createAccountData();
		return new Result(true, null, "success!");
	}
	@GetMapping("/myerror")
	public Result error() {
		throw new RuntimeException("my error!");
	}
}
