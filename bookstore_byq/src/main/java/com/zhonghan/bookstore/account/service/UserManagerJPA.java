package com.zhonghan.bookstore.account.service;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.account.model.Permission;
import com.zhonghan.bookstore.account.model.Role;
import com.zhonghan.bookstore.account.model.RolePermissionRelation;
import com.zhonghan.bookstore.account.model.User;
import com.zhonghan.bookstore.account.model.UserRoleRelation;
import com.zhonghan.bookstore.product.model.Product;
@Service
@Primary
public class UserManagerJPA  {



	@PersistenceContext EntityManager em;
	//String sql = "CREATE TABLE PRODUCT ( id VARCHAR(100), name VARCHAR(100), description VARCHAR(100), createTime DATE, price DOUBLE(10, 2) NOT NULL DEFAULT '0.00',PRIMARY KEY (id))";
	@Transactional
	public User createUser(User p) {
		em.persist(p);
		return p;
	}
	@Transactional
	public void deleteUser(String pid) {
		Object p = em.find(User.class, pid);
		em.remove(p);

	}
	@Transactional
	
	public User update(User p) {
		// TODO Auto-generated method stub
		em.persist(p);
		return p;
	}

	
	public List<User> search(String key) {
		// TODO Auto-generated method stub
		
		TypedQuery<User> query = em.createQuery("from User where name like  '%" + key + "%' or home like " + "'%" + key + "%'", User.class );
		List<User> rst = query.getResultList();
		return rst;
	}
	
	public boolean exist(String userName, String password) {
		
		TypedQuery<User> query = em.createQuery("from User where name='" + userName + "' and password='" + password + "'", User.class);
		List<User> data = query.getResultList();
		return !data.isEmpty();
	}

	public User fetch(String userName, String password) {
		
		TypedQuery<User> query = em.createQuery("from User where name='" + userName + "' and password='" + password + "'", User.class);
		List<User> data = query.getResultList();
		if(data.isEmpty())
			return null;
		
		return data.get(0);
	}
	
	
	public List<String> extractPermissions(String userId){
		
		 List<String> rst = new ArrayList<String>();
		 String ql_role = "from UserRoleRelation where userId = '" + userId + "'";
		 
		 
		TypedQuery<UserRoleRelation> query = em.createQuery(ql_role, UserRoleRelation.class);
		List<UserRoleRelation> roles = query.getResultList();
		
		for(UserRoleRelation r : roles) {
			 String ql_per = "select permissionId from RolePermissionRelation where roleId = '" + r.getRoleId() + "'";

				TypedQuery<String> query1 = em.createQuery(ql_per, String.class);
				List<String> ps = query1.getResultList();
				rst.addAll(ps);
				
		}
		
		return rst;
		
	}
	

	@Transactional
	public void createAccountData() {
		
		Permission p2 = new Permission();
		p2.setId("product:add");
		p2.setName("产品搜索");
		em.persist(p2);
		
		Permission p = new Permission();
		p.setId("product:update");
		p.setName("产品搜索");
		em.persist(p);
		
		
		Permission p1 = new Permission();
		p1.setId("product:remove");
		p1.setName("产品shanchu");
		em.persist(p1);
		
		
		
		Role role = new Role();
		role.setId("buyer");
		role.setName("读者");
		em.persist(role);
		
		User user = new User();
		user.setId("user1");
		user.setName("user1");
		user.setPassword("1234");
		em.persist(user);
		
		
		UserRoleRelation url = new UserRoleRelation();
		url.setId(UUID.randomUUID().toString());
		url.setUserId(user.getId());
		url.setRoleId(role.getId());
		em.persist(url);
		
		
		
		
		
		RolePermissionRelation rpl = new RolePermissionRelation();
		rpl.setId(UUID.randomUUID().toString());
		rpl.setRoleId(role.getId());
		rpl.setPermissionId(p.getId());
		em.persist(rpl);
		
		RolePermissionRelation rpl1 = new RolePermissionRelation();
		rpl1.setId(UUID.randomUUID().toString());
		rpl1.setRoleId(role.getId());
		rpl1.setPermissionId(p1.getId());
		
		em.persist(rpl1);
		
		
		
		RolePermissionRelation rpl2 = new RolePermissionRelation();
		rpl2.setId(UUID.randomUUID().toString());
		rpl2.setRoleId(role.getId());
		rpl2.setPermissionId(p2.getId());
		em.persist(rpl2);
		

		

		
	}

}
