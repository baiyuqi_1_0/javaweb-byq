package com.zhonghan.bookstore.account.model;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class RolePermissionRelation {
	@Id
	String id;
	String roleId;
	String permissionId;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(String permissionId) {
		this.permissionId = permissionId;
	}
	

}
