package com.zhonghan.bookstore.account.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserRoleRelation {
	@Id
	String id;
	String userId;
	String roleId;
	public String getId() {
		return id;
	}
	public String getRoleId() {
		return roleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
