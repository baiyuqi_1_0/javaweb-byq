package com.zhonghan.bookstore.account.model;

import java.util.List;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="MYUSER")
public class User {
	@Id
	String id;
	String name;
	String password;
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Embedded
	Address address;
	@Embedded
	Contact contact;
	@Transient
	public List<Role> roles;
	
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public Address getAddress() {
		return address;
	}
	public Contact getContact() {
		return contact;
	}
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public void setContact(Contact contact) {
		this.contact = contact;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
}
