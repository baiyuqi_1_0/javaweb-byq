package com.zhonghan.bookstore.account.model;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
	String home;
	String work;
	public String getHome() {
		return home;
	}
	public void setHome(String home) {
		this.home = home;
	}
	public String getWork() {
		return work;
	}
	public void setWork(String work) {
		this.work = work;
	}
	

}
