package com.zhonghan.bookstore.account.service;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.account.model.Role;
import com.zhonghan.bookstore.account.model.User;
import com.zhonghan.bookstore.account.model.UserRoleRelation;
import com.zhonghan.bookstore.business.model.Cart;
import com.zhonghan.bookstore.business.service.CartService;
import com.zhonghan.bookstore.security.Result;

@Service
public class AccountService {
	@PersistenceContext EntityManager em;
	@Autowired UserManagerJPA userManager;
	@Autowired CartService cartService;
	
	@Transactional
	public User signup(User user) {
		user.setId(user.getName());

		
		userManager.createUser(user);
		Cart cart = new Cart();
		cart.setId(user.getId());
		cartService.createCart(cart);
		assignRole(Role.BUYER, user.getId());
		
		
		return user;
	
	}
	
	
	 void assignRole(String roleId, String userId) {
		 
		 UserRoleRelation url = new UserRoleRelation();
		 url.setRoleId(roleId);
		 url.setUserId(userId);
		 url.setId(userId + "@" + roleId);
		 em.persist(url);
		
	}

}
