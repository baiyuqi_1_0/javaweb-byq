package com.zhonghan.bookstore.account.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.zhonghan.bookstore.account.model.User;
import com.zhonghan.bookstore.account.service.AccountService;
import com.zhonghan.bookstore.account.service.UserManagerJPA;
import com.zhonghan.bookstore.business.model.Cart;
import com.zhonghan.bookstore.business.service.CartService;
import com.zhonghan.bookstore.security.Result;
import com.zhonghan.bookstore.security.Util;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired ApplicationContext container;
	@Autowired UserManagerJPA manager;
	@Autowired CartService cartService;
	
	@Autowired AccountService accountService;

	//http://localhost:8080/product/add?name="西游记"&price=123&description=四大名著之一
	@GetMapping("signup")
	public Result createUser(User p) {
		accountService.signup(p);

		Result rst = new Result(true, p, "add User success!");

		return rst;
	}
	//http://localhost:8080/product/update?name="西游记"&price=123&description=四大名著之一
	@GetMapping("update")
	public Result updateUser(User p) {

		
		manager.update(p);

		Result rst = new Result(true, p, "update User success!");

		return rst;
	}
	@GetMapping("remove")
	public Result deleteProduct(String userId) {
	
		manager.deleteUser(userId);

		Result rst = new Result(true, null, "delete User success!");

		return rst;
	}
	@GetMapping("search")
	public Result<List<User>> search(String key) {

		List<User> ps = manager.search(key);
		 Result<List<User>> rst = new  Result<List<User>>(true, ps, "search User success!");

		return rst;
	}

}
