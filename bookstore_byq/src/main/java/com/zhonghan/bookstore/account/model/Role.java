package com.zhonghan.bookstore.account.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Role {
	public static final String BUYER = "buyer"; 
	@Id
	String id;
	String name;
	public String getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
