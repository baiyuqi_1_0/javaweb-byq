package com.zhonghan.bookstore.account.model;

import javax.persistence.Embeddable;

@Embeddable
public class Contact {
	String phone;
	String weixin;
	String qq;
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getWeixin() {
		return weixin;
	}
	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}
	public String getQq() {
		return qq;
	}
	public void setQq(String qq) {
		this.qq = qq;
	}

}
