package com.zhonghan.bookstore.business.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
@Entity
public class Cart implements Serializable{
	@Id
	String id;
	
	
	public String getId() {
		return id;
	}
	public void setId(String cid) {
		id = cid;
	}
	@Transient
//	@OneToMany
	List<CartItem> items = new ArrayList<CartItem>();
	public List<CartItem> getItems() {
		return items;
	}
	public void setItems(List<CartItem> items) {
		this.items = items;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	double totalCost;
	public void  add(CartItem i) {
		items.add(i);
	}
	public void  remove(String pid) {
		for(CartItem item : items) {
			if(item.getProductId().equals(pid)) {
				items.remove(item);
			}
		}
		
		
	}

}
