package com.zhonghan.bookstore.business.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhonghan.bookstore.business.model.Cart;
import com.zhonghan.bookstore.business.model.CartItem;
import com.zhonghan.bookstore.business.service.CartService;
import com.zhonghan.bookstore.security.Result;

@RestController
@RequestMapping("cart")
public class CartController {
	@Autowired CartService service;
	@GetMapping("/add")
	public Result<Cart> addItem(CartItem item){
		service.saveItem(item);
		Result<Cart> rst = new Result<Cart>(false, null, "add cart success!");
		return rst;
		
	}
	@GetMapping("/view")
	public Result<Cart> view(){
	
		
		String cartId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		Cart cart = 	service.getCart(cartId);
		Result<Cart> rst = new Result<Cart>(false, cart, "view cart success!");
		return rst;
		
	}
	@GetMapping("/remove")
	public Result<Cart> remove(String itemId){
		Result<Cart> rst = new Result<Cart>(false, null, "remove cart success!");
		service.remove(itemId);
		return rst;
		
	}
}
