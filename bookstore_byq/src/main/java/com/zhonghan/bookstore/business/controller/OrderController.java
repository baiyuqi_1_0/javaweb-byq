package com.zhonghan.bookstore.business.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhonghan.bookstore.business.model.Cart;
import com.zhonghan.bookstore.business.model.CartItem;
import com.zhonghan.bookstore.business.model.Order;
import com.zhonghan.bookstore.business.model.OrderItem;
import com.zhonghan.bookstore.business.service.CartService;
import com.zhonghan.bookstore.business.service.OrderService;
import com.zhonghan.bookstore.security.Result;

@RestController
@RequestMapping("order")
public class OrderController {
	
	@Autowired OrderService service;
	
	@PostMapping("/create")
	public Result<Order> createOrders(@RequestBody CartItem[] items){
		Order order = extractOrder(items);
		service.createOrder(order);
		Result<Order> rst = new Result<Order>(false, null, "下单  success!");
		return rst;
		
	}
	private Order extractOrder(CartItem[] items) {
		Order rst = new Order();
		rst.setId(UUID.randomUUID().toString());
		List<OrderItem> oitems = transformItems(items, rst);
		rst.setItems(oitems);
		return rst;
	}
	private List<OrderItem> transformItems(CartItem[] items, Order order) {
		List<OrderItem> rst = new ArrayList<OrderItem>();
		for(CartItem i : items) {
			OrderItem oi = transform(i, order);
			rst.add(oi);
		}
		return rst;
	}
	private OrderItem transform(CartItem i, Order order) {
		OrderItem rst = new OrderItem();
		rst.setOrderId(order.getId());
		rst.setId(i.getId());
		rst.setProductId(i.getProductId());
		rst.setProductName(i.getProductName());
		rst.setPrice(i.getPrice());
		rst.setCost(i.getCost());
		return rst;
	}
}
