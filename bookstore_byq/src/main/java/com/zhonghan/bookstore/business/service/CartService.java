package com.zhonghan.bookstore.business.service;

import java.util.List;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.business.model.Cart;
import com.zhonghan.bookstore.business.model.CartItem;

@Service
public class CartService {
	@PersistenceContext EntityManager em;
	@Transactional
	public void saveItem(CartItem item) {
		item.setId(UUID.randomUUID().toString());
		
		String currentUserId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		item.setCartId(currentUserId);
		em.persist(item);
		
		
	}
	public Cart getCart(String cartId) {
		Cart cart = em.find(Cart.class, cartId);
		
		TypedQuery<CartItem> query = em.createQuery("from CartItem where cartId = '" + cartId + "'", CartItem.class);
		List<CartItem> items = query.getResultList();
		cart.setItems(items);
		return cart;
	}
	@Transactional
	public void remove(String itemId) {
		// TODO Auto-generated method stub
		
	}
	@Transactional
	public void createCart(Cart cart) {
		em.persist(cart);
		
	}

}
