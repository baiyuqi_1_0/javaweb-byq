package com.zhonghan.bookstore.business.service;

import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.business.model.Cart;
import com.zhonghan.bookstore.business.model.CartItem;
import com.zhonghan.bookstore.business.model.Order;
import com.zhonghan.bookstore.business.model.OrderItem;
import com.zhonghan.bookstore.pay.controller.Trade;

@Service
public class OrderService {
	@PersistenceContext EntityManager em;
	
	
	
	@Transactional
	public void createOrder(Order order) {
		
		String currentUserId = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		order.setBuyer(currentUserId);
		em.persist(order);
		for(OrderItem i : order.getItems()) {
			em.persist(i);
		}
		Trade trade = new Trade();
		trade.setOrderId(order.getId());
		trade.setTradeNo(Trade.createTradeNo());
		em.persist(trade);
		
	}
	public Order findOrder(String orderId) {
		// TODO Auto-generated method stub
		return em.find(Order.class, orderId);
	}
	public Trade findTrade(String tradeId) {
		// TODO Auto-generated method stub
		return em.find(Trade.class, tradeId);
	}
	public void finishTrade(String tradeId) {
		
		Trade trade = findTrade(tradeId);
		trade.setFinisahed(true) ;
		em.merge(trade);
		
	}
	public Trade findTradeByOrder(String orderId) {
		TypedQuery<Trade> q = em.createQuery("from Trade where orderId = '" + orderId + "'", Trade.class);
		
		return q.getSingleResult();
	}

}
