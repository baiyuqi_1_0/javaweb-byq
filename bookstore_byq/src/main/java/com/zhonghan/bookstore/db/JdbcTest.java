package com.zhonghan.bookstore.db;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class JdbcTest {

	public static void main(String[] args) throws Exception {

		String url = "jdbc:mysql://localhost:3306/mysql";
		String user = "root";
		String password = "root";

		// 1. 选择数据库：加载数据库驱动
		//Class driverType = Class.forName("com.mysql.cj.jdbc.Driver");
		// 2. 连接数据库
		Connection conn = DriverManager.getConnection(url, user, password);
		
		PreparedStatement ps = conn.prepareStatement("INSERT INTO PRODUCT(id,name) VALUES(?,?)");
		
		ps.setInt(1, 1);
		ps.setString(2, "book");
		
		
		// 4. 获取查询结果
		int i = ps.executeUpdate();
		System.out.println("一共执行了" + i + "条");
	}

}
