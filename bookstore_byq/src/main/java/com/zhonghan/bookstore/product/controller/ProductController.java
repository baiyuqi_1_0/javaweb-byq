package com.zhonghan.bookstore.product.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zhonghan.bookstore.product.model.Product;
import com.zhonghan.bookstore.product.service.ProductService;
import com.zhonghan.bookstore.security.Result;
import com.zhonghan.bookstore.security.Util;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired ApplicationContext container;
	@Autowired ProductService manager;

	//http://localhost:8080/product/add?name="西游记"&price=123&description=四大名著之一
	
	
	
	@PreAuthorize(value = "hasAuthority('product:add')")
	@GetMapping("add")
	public Result createProduct(Product p) {
		p.setId(UUID.randomUUID().toString());

	
		manager.createProduct(p);

		Result rst = new Result(true, p, "add product success!");

		return rst;
	}
	//http://localhost:8080/product/update?name="西游记"&price=123&description=四大名著之一
	@PreAuthorize(value = "hasAuthority('product:update')")
	@GetMapping("update")
	public Result updateProduct(Product p) {

		
		manager.update(p);

		Result rst = new Result(true, p, "update product success!");

		return rst;
	}
	@PreAuthorize(value = "hasAuthority('product:remove')")
	@GetMapping("remove")
	public Result deleteProduct(String productId) {
	
		manager.deleteProduct(productId);

		Result rst = new Result(true, null, "delete product success!");

		return rst;
	}

	@GetMapping("search")
	public Result<List<Product>> search(String key) {

		List<Product> ps = manager.search(key);
		 Result<List<Product>> rst = new  Result<List<Product>>(true, ps, "search product success!");

		return rst;
	}

}
