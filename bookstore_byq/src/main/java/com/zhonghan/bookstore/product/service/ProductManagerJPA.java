package com.zhonghan.bookstore.product.service;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.product.model.Product;
@Service
@Primary
public class ProductManagerJPA implements ProductService {



	@PersistenceContext EntityManager em;
	//String sql = "CREATE TABLE PRODUCT ( id VARCHAR(100), name VARCHAR(100), description VARCHAR(100), createTime DATE, price DOUBLE(10, 2) NOT NULL DEFAULT '0.00',PRIMARY KEY (id))";
	@Transactional
	public Product createProduct(Product p) {
		em.persist(p);
		return p;
	}
	@Transactional
	public void deleteProduct(String pid) {
		Object p = em.find(Product.class, pid);
		em.remove(p);

	}
	@Transactional
	@Override
	public Product update(Product p) {
		// TODO Auto-generated method stub
		em.persist(p);
		return p;
	}

	@Override
	public List<Product> search(String key) {
		// TODO Auto-generated method stub
		if(key== null)
			key = "";
		TypedQuery<Product> query = em.createQuery("from Product where name like  '%" + key + "%'", Product.class);
		List<Product> rst = query.getResultList();
		return rst;
	}

}
