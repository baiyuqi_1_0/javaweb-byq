package com.zhonghan.bookstore.product.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Brand {
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="bandname")
	String name;
	String logo;
	@Column(name="banddesc")
	String description;

}
