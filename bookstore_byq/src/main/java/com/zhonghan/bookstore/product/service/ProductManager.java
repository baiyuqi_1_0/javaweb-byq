package com.zhonghan.bookstore.product.service;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.product.model.Product;
@Service
public class ProductManager implements ProductService {



	@Autowired DataSource ds;
	//String sql = "CREATE TABLE PRODUCT ( id VARCHAR(100), name VARCHAR(100), description VARCHAR(100), createTime DATE, price DOUBLE(10, 2) NOT NULL DEFAULT '0.00',PRIMARY KEY (id))";
	
	/* (non-Javadoc)
	 * @see com.zhonghan.bookstore.db.ProductService#createProduct(com.zhonghan.bookstore.product.Product)
	 */
	@Override
	public Product createProduct(Product p) {
		try {
			
			Connection connection = ds.getConnection();
			PreparedStatement ps = connection.prepareStatement("INSERT INTO PRODUCT(id,name, description, createTime, price) VALUES(?,?,?,?,?)");

			ps.setString(1, UUID.randomUUID().toString());
			ps.setString(2, p.getName());
			ps.setString(3, p.getDescription());
			ps.setDate(4, new Date(System.currentTimeMillis()));
			ps.setDouble(5,p.getPrice());

			// 4. 获取查询结果
			int i = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		return p;
	}

	/* (non-Javadoc)
	 * @see com.zhonghan.bookstore.db.ProductService#deleteProduct(java.lang.String)
	 */
	@Override
	public void deleteProduct(String pid) {
		try {
			Connection connection = ds.getConnection();
			PreparedStatement ps = connection.prepareStatement("delete from PRODUCT where id = ?");

			ps.setString(1, pid);
			

			// 4. 获取查询结果
			int i = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}

	}

	/* (non-Javadoc)
	 * @see com.zhonghan.bookstore.db.ProductService#update(com.zhonghan.bookstore.product.Product)
	 */
	@Override
	public Product update(Product p) {
		try {
			Connection connection = ds.getConnection();
			PreparedStatement ps = connection.prepareStatement("update  PRODUCT set name=? and description=? and price=? where id = ? ");


			ps.setString(2, p.getName());
			ps.setString(3, p.getDescription());
			ps.setDouble(4,p.getPrice());
			ps.setString(1, p.getId());
			// 4. 获取查询结果
			int i = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		return p;
	}

	/* (non-Javadoc)
	 * @see com.zhonghan.bookstore.db.ProductService#search(java.lang.String)
	 */
	@Override
	public List<Product> search(String key) {
		if(key == null)
			key = "";
		List<Product> rst = new ArrayList<Product>();
		try {
			Connection connection = ds.getConnection();
			Statement ps = connection.createStatement();
			ResultSet i = ps.executeQuery("select * from PRODUCT where name like '%" + key + "%' || description  like '%" + key + "%'");
		
			while(i.next()) {
				String id = i.getString("id");
				String name = i.getString("name");
				String desc = i.getString("description");
				Date cd = i.getDate("createTime");
				double price = i.getDouble("price");
				
				
				Product p = new Product();
				p.setId(id);
				p.setName(name);
				p.setDescription(desc);
				p.setCreateTime(cd);
				p.setPrice(price);
				rst.add(p);
				
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
		}
		return rst;
	}

//	@PostConstruct
	public void createProductTableIfNotExist() {

		try {
			Connection connection = ds.getConnection();
			Statement ps = connection.createStatement();
			ps.executeUpdate("CREATE TABLE PRODUCT ( id VARCHAR(100), name VARCHAR(100), description VARCHAR(100), createTime DATE, price DOUBLE(10, 2) NOT NULL DEFAULT '0.00',PRIMARY KEY (id))");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

}
