package com.zhonghan.bookstore.product.service;

import java.util.List;

import com.zhonghan.bookstore.product.model.Product;

public interface ProductService {

	Product createProduct(Product p);

	void deleteProduct(String pid);

	Product update(Product p);

	List<Product> search(String key);

}