package com.zhonghan.bookstore.product.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Category {
	@Column(name="category_name")
	String name;
	@Column(name="category_description")
	String description;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
