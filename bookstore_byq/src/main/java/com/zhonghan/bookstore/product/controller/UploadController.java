package com.zhonghan.bookstore.product.controller;

import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.zhonghan.bookstore.product.service.StorageService;

@RestController
public class UploadController {
	@Autowired StorageService service;
	@PostMapping(value = "/uploadImage", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public String uploadImage(@RequestParam("file") MultipartFile file, String productId) {
			if (file.isEmpty()) {
				return null;
			}
			String storedFileId = service.store(file);
			
	//		this.getProductService() .storeProductImage(productId, storedFileId);
			
			return null;
	    }

	@GetMapping(value = "/view")
	public void view(HttpServletResponse resp, String documentId) throws IOException {
		if (documentId == null || documentId.equals("null") || documentId.endsWith(".pdf")) {
			documentId = "dummy.jpg";
		}
		// byte[] data = storageService.read(documentId);
		// out.write(data);
		ServletOutputStream out = resp.getOutputStream();
		// resp.setContentType("application/pdf");
		service.read(out, documentId);

	}
}
