package com.zhonghan.bookstore.log.service;

import java.util.Date;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.zhonghan.bookstore.log.model.Log;

@Service
public class LogService {
	@PersistenceContext EntityManager em;
	
	@Transactional
	public void saveLog(Log log) {
		log.setLogTime(new Date());
		log.setId(UUID.randomUUID().toString());
		em.persist(log);
	}

}
