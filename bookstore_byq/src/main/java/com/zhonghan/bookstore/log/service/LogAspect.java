package com.zhonghan.bookstore.log.service;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.zhonghan.bookstore.log.model.Log;
import com.zhonghan.bookstore.security.Result;

@Aspect
@Component
public class LogAspect {
	@Autowired
	LogService logService;
	
	
	@Pointcut("@annotation(org.springframework.web.bind.annotation.GetMapping)||@annotation(org.springframework.web.bind.annotation.PostMapping)")
	void LogPoint() {}
	
	
	@Around(value = "LogPoint()")
	public Object logIt(ProceedingJoinPoint jp) throws Throwable {
		Object[] args = jp.getArgs();
		
		Log log = new Log();
		log.setMethodName(jp.getSignature().toString());
		Object rst = null;
		try {
			rst = jp.proceed(args);
			log.setSuccess(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.setErrorMessage(e.getMessage());
			log.setSuccess(false);
			rst = new Result(false, null, e.getMessage());
			
		}
		
		
		
		
		String name =  (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		log.setUser(name);
		log.setParams(args.toString());
		
		logService.saveLog(log);
		
		//
		
		//exception
		return rst;
		
	}

}