package com.zhonghan.bookstore.pay.controller;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Trade {
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	String orderId;
	@Id
	String tradeNo;
	double amount;
	String subject;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	boolean finisahed = false;
	public boolean isFinisahed() {
		return finisahed;
	}
	public void setFinisahed(boolean finisahed) {
		this.finisahed = finisahed;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getTradeNo() {
		return tradeNo;
	}
	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}
	static public String createTradeNo() {
		return "202312148534585s";
	}

}
