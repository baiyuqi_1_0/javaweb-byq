package com.zhonghan.bookstore.pay.controller;
public class AlipayConfig {
    // 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号
    public static String app_id = "9021000132678274";
    // 商户私钥，您的PKCS8格式RSA2私钥
    public static String merchant_private_key="MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCkxjgJVPp/V7wIpEEh+5nJqgmPVH/4LQGej9t0dsjR06p2YqmP+PqukAORcbUqU4Pjd2GSjHP8wkdYELw7wurEShLRt5fXO3bPc5etjtOhQhMd6y7m0oC6rivCOsRypW66YWlWqbMeOs+JGkKFd65wPjUSQBV7tAlgLJ0uog3kQS0JwdkBH96R54IksX0E/xPb0IGYmoHLlx7AX9iP4gdwJ3ELx7eTZUYgv5O3pkhvouiVmbt4EU0acjL29qmYSGK2d8wFd+KV+rZhwgdKkgd7Mrb1O0JYapujmFhDkqxE4r5/6Vtr0GOsWQCmPjhnaJbpDmtV1GBhRTHTv9CyB+SlAgMBAAECggEBAIuTqUYmaOdUD2EWmmHJ+jS8x8pVHu3AWh3R6GkYcfZkIYKVAuOFWfxi29LpijgndJJLvyhft6TFgZ/U1U6eRzYtNRHXPlVA9ty2IycO1LBGA2GVpi465Ha+MS0fkOc4wbo55/xUVflmggS2bOEYR3inCVfDVy9pi4OfZypodfIB8BiOvNq+WgrMn5gBzZCB0jKREubB8YLHAcMLg5/d7MDzWKsuU9Qmc8ah0i7KSW82NLG6v1k2q/83odxsWzBIpXuqx0nfBMi5Y/+NK9KIdskk4ixnCen2yDLxPxd2spnf4QIla7Xe4weful+YhRC9P+c5ASIPS/WzbgTclVD37KUCgYEA7ZU1DzymOiwbhS14fKOON609EXTScOZiY+Uam2qT0SepJrkhH3Dis6RHIAkQQJtSUo++IRT+0dXBFRj5d1IvmKoGAAAb+56sT07bs72cV7uIrzQwmEaFqybn1YNFsLb389W7DYHqaoC/SLadue4I/gGAw3wM+ZAFQ5txZGwhQ2cCgYEAsYwjSsJOjyXUWfJHBtERAUYjhcunhubH2mJaxCfSHzaMzwRgoD+ks4a5M3Au0WT9JM89H7cyf0yWlk1MFbZxvlt7bPpJIrLAvPX2bXMNPpjHLo/f0ZR/sjPejhngojhoCESUznRDpPuccHCfBRxpVDPC3oN3SvqD2kdwJSGGfBMCgYAOBL7TvlVkm1OoYQ159UCgAmXXENiV9tIZaNxQDVLZhYiSk1vj+go2ZVwZBM/jsAaEfpzOYOnEQeOZthaKNtw+GtItz9PW3f720wVdrrN57cPb6q1vXsHmXm5Rh+Rlz/YDejoGSwMk86HVzYiy/tui+T26yygM1bWpzpM7RLIVwwKBgH8YdEMV6Sqrx+FDf/ivPF8PGGq0qI33MmVwNs1oPBA/+nhPB+Ru6rXg4T6jNcCLbHMnLvg/KhTafInHd4mreqzry+TtHeGK2FHsOKWM0JRYxWzdbTkX9nlYjB5pA9bcTgxtG6DWtX2q6AfNdugvuG6gRwUGUyUHQ8/Dh9Hst9qlAoGAdxFzfOE5AMAH3ARLOJSe7xxuhzvE+iVJMtI88V6J8mmKjYCXeTECsDILXtCT/4E4eBn8bzRDSslX82TiY3SVvL0E6r5KJXUW2XbsCLa0hOC2JgLGIFsXviK+5SjwiqI85UEbujC9e74jdJOBdqrAzZ/6Pr5YJRAL5ZGfqSptCC4=";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    public static String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAifMXjsPC210EC6gweDCnvl0pEbOqFqpGF8SLllBxE8upuV/iRmfIIhhIZAc8WyWWXXTfDnVJfsZs+oivmbZOmXTUF7iMrjn0vKVOL4Y2o5RF3iz4vrT4HSHU0r6L3m64/l9d3P2WsVj1GzJnteXYm8TcbyWtWGAlrUvVrAV+IYDirLr9VsTz38fFaE4kzbnRWWEyPUIyj5NmWhIsp/dYQDW8zBkxQ7LU0L8VLsIcUQMeMjX3J4eyh56zMnqUR0FTrFzmHp7e/DWbMKuHGF3oK+4mJK6KYVtL4xu8LaaSTVb6DN59Rf/fuayJkbCh0h+Szxif7lA149m1CAcYy/N8kwIDAQAB";
    // 服务器异步通知页面路径
    //需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String notify_url = "http://public_address:8080/notify";
    // 页面跳转同步通知页面路径
    //需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    public static String return_url = "http://localhost:8080/index.html";
    // 签名方式
    public static String sign_type = "RSA2";
    // 字符编码格式
    public static String charset = "utf-8";
    // 支付宝网关,注意这些使用的是沙箱的支付宝网关，与正常网关的区别是多了dev
    public static String gatewayUrl = "https://openapi-sandbox.dl.alipaydev.com/gateway.do";
 
}