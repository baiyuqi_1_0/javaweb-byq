package com.zhonghan.bookstore.pay.controller;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
/**
 * 提供支付宝相关的一些对象
 */
@Configuration
public class AliBean {
    //创建支付宝需要的客户端对象
    @Bean
    public AlipayClient alipayClient(){
        return new DefaultAlipayClient(AlipayConfig.gatewayUrl,AlipayConfig.app_id,AlipayConfig.merchant_private_key,"json",AlipayConfig.charset,
        		AlipayConfig.alipay_public_key,AlipayConfig.sign_type);
    }
    //创建一个支付宝的请求对象
    @Bean
    public AlipayTradePagePayRequest alipayTradePagePayRequest(){
     return new AlipayTradePagePayRequest();
    }
}