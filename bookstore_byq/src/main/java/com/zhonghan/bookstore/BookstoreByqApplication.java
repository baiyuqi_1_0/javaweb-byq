package com.zhonghan.bookstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
//@ServletComponentScan(basePackages="com.zhonghan.bookstore")
public class BookstoreByqApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookstoreByqApplication.class, args);
	}

}
