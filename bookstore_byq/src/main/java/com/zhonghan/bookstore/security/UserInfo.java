package com.zhonghan.bookstore.security;

import java.util.List;

public class UserInfo {
	String userName;
	List<String> perssions;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<String> getPerssions() {
		return perssions;
	}
	public void setPerssions(List<String> perssions) {
		this.perssions = perssions;
	}

}
