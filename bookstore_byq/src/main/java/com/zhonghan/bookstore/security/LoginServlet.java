package com.zhonghan.bookstore.security;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/loginservlet")
public class LoginServlet extends HttpServlet {
//http://localhost:8080/loginservlet?userName=byq&password=123
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		super.init();
	}

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HttpSession session = req.getSession(true);
		Object userInfo = session.getAttribute("userInfo");
		if (userInfo == null) {

		UserInfo myInfo = new UserInfo();
		String userName = req.getParameter("userName");
		String password = req.getParameter("password");
		//   取数据库，判断是否合法用户
		
		myInfo.setUserName(userName);

		session.setAttribute("userInfo", myInfo);
		userInfo = myInfo;
		}
		
		
		Result rst = new Result(true, userInfo, "login success!");
		String ui = Util.toJson(rst);

		 PrintWriter out = resp.getWriter();
		 out.print(ui);

		out.flush();
		out.close();

		/*
		 * resp.getWriter().write("jjjj"); resp.getWriter().flush();
		 * 
		 * resp.getWriter().close();
		 */

	}

	@Override
	protected void doPost(HttpServletRequest req,

			HttpServletResponse resp) throws ServletException, IOException {

		this.doGet(req, resp);

	}

}