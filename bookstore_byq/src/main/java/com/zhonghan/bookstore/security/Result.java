package com.zhonghan.bookstore.security;

public class Result<T> {
	boolean success;
	T data;
	String message;
	
	
	
	public Result(boolean success, T data, String message) {
		super();
		this.success = success;
		this.data = data;
		this.message = message;
	}
	public T getData() {
		return data;
	}
	public String getMessage() {
		return message;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setData(T data) {
		this.data = data;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}

}
