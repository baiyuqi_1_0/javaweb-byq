package com.zhonghan.bookstore.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Util {
	static public String toJson(Object data) {
		 ObjectMapper objectMapper = new ObjectMapper();


         String json;
		try {
			json = objectMapper.writeValueAsString(data);
	         return json;
	 		
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

}
