package com.zhonghan.bookstore.security;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@WebFilter({"/cartservlet" })
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpSession session = req.getSession(true);
		Object userInfo = session.getAttribute("userInfo");

		if (userInfo != null) {
			chain.doFilter(req, response);
		} else {

			Result rst = new Result(false, null, "Access denied!");
			String ui = Util.toJson(rst);

			PrintWriter out = response.getWriter();
			out.print(ui);

			out.flush();
			out.close();
		}

	}

}
